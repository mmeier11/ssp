package de.meierit.ssp.ssp.business;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import de.meierit.ssp.ssp.application.WinningResponse;
import de.meierit.ssp.ssp.domain.Gesture;
import de.meierit.ssp.ssp.domain.Winner;

@Service
public class LogicService {
		
	private final EnumSet<Gesture> BASIC_GESTURES = EnumSet.of(Gesture.PAPER, Gesture.SCISSORS, Gesture.ROCK);
	private final EnumSet<Gesture> ADVANCES_GESTURES = EnumSet.allOf(Gesture.class);
	
	private Random random = new Random();
	
	private LogicHolder logicHolder = new LogicHolder();
	
	@PostConstruct
	public void init() {
		logicHolder.buildFor(Gesture.PAPER).beats(Gesture.ROCK).beats(Gesture.WELL).add();
		logicHolder.buildFor(Gesture.SCISSORS).beats(Gesture.PAPER).add();
		logicHolder.buildFor(Gesture.ROCK).beats(Gesture.SCISSORS).add();
		logicHolder.buildFor(Gesture.WELL).beats(Gesture.ROCK).beats(Gesture.SCISSORS).add();
	}
	
	public WinningResponse playBasic(Gesture humanGesture) {	
		return play(humanGesture, BASIC_GESTURES);
	}
	
	public WinningResponse playAdvanced(Gesture humanGesture) {
		return play(humanGesture, ADVANCES_GESTURES);	
	}
	
	WinningResponse play(Gesture humanGesture, EnumSet<Gesture> gestures) {		
		if (usedNotAllowedGesture(humanGesture, gestures)) {
			throw new GestureNotAllowedException(humanGesture);
		}
		
		Gesture kiGesture = getKiGesture(gestures);
		if (isDrawn(humanGesture, kiGesture)) {
			return new WinningResponse(Winner.DRAWN, humanGesture, kiGesture);
		}
		
		Gesture winningGesture = logicHolder.getWinningGestureOf(humanGesture, kiGesture);
		if (isHumanTheWinner(humanGesture, winningGesture)) {
			return new WinningResponse(Winner.HUMAN, humanGesture, kiGesture);
		}
		
		return new WinningResponse(Winner.KI, humanGesture, kiGesture);
	}

	private boolean usedNotAllowedGesture(Gesture humanGesture, EnumSet<Gesture> gestures) {
		return !gestures.contains(humanGesture);
	}

	private boolean isHumanTheWinner(Gesture humanGesture, Gesture winningGesture) {
		return humanGesture == winningGesture;
	}
	
	private boolean isDrawn(Gesture humanGesture, Gesture kiGesture) {
		return isHumanTheWinner(humanGesture, kiGesture);
	}

	Gesture getKiGesture(EnumSet<Gesture> playingGestures) {
		return (Gesture) Arrays.asList(playingGestures.toArray()).get(random.nextInt(playingGestures.size()));
	}

}
