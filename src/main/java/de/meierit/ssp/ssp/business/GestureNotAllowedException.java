package de.meierit.ssp.ssp.business;

import de.meierit.ssp.ssp.domain.Gesture;

public class GestureNotAllowedException extends RuntimeException {

	private static final long serialVersionUID = 1786106930035332886L;
	
	private final Gesture usedGesture;

	public GestureNotAllowedException(Gesture usedGesture) {
		this.usedGesture = usedGesture;
	}

	public Gesture getUsedGesture() {
		return usedGesture;
	}

}
