package de.meierit.ssp.ssp.business;

import java.util.HashMap;
import java.util.Map;

import de.meierit.ssp.ssp.domain.Gesture;
import de.meierit.ssp.ssp.domain.GesturePair;

public class LogicHolder {

	private Map<GesturePair, Gesture> winningGestures = new HashMap<>();

	public Gesture getWinningGestureOf(Gesture first, Gesture second) {
		return winningGestures.get(new GesturePair(first, second));
	}

	public LogicBuilder buildFor(Gesture gesture) {
		return new LogicBuilder(this, gesture);
	}

	void addLogic(Gesture first, Gesture second, Gesture winning) {
		winningGestures.put(new GesturePair(first, second), winning);
	}

}
