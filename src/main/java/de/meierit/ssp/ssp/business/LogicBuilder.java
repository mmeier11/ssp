package de.meierit.ssp.ssp.business;

import java.util.HashSet;
import java.util.Set;

import de.meierit.ssp.ssp.domain.Gesture;

public class LogicBuilder {

	private final Gesture winning;
	private Set<Gesture> winnsAgainst = new HashSet<Gesture>();
	private LogicHolder holder;	
	
	public LogicBuilder(LogicHolder holder, Gesture winning) {
		this.holder = holder;
		this.winning = winning;
	}
	
	public LogicBuilder beats(Gesture beats) {
		winnsAgainst.add(beats);
		return this;
	}
	
	public void add() {
		winnsAgainst.stream().forEach(gesture -> addToHolder(gesture));		
	}

	private void addToHolder(Gesture gesture) {
		holder.addLogic(winning, gesture, winning);
		holder.addLogic(gesture, winning, winning);
	}
}
