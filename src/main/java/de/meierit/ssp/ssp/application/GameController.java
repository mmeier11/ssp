package de.meierit.ssp.ssp.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.meierit.ssp.ssp.business.LogicService;

@RestController
@RequestMapping(path = "/api/game")
public class GameController {

	@Autowired
	private LogicService service;

	@PostMapping
	public WinningResponse playBasic(@RequestBody Play play) {
			return service.playBasic(play.getGesture());
	}
	
	@PostMapping(path="/advanced")
	public WinningResponse playAdvanced(@RequestBody Play play) {
			return service.playAdvanced(play.getGesture());
	}

}
