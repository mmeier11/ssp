package de.meierit.ssp.ssp.application;

import de.meierit.ssp.ssp.domain.Gesture;

public class Play {

	private Gesture gesture;
	
	public Play() {
	}

	public Play(Gesture gesture) {
		this.gesture = gesture;
	}

	public Gesture getGesture() {
		return gesture;
	}

	public void setGesture(Gesture gesture) {
		this.gesture = gesture;
	}

}
