package de.meierit.ssp.ssp.application;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import de.meierit.ssp.ssp.business.GestureNotAllowedException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { GestureNotAllowedException.class })
	protected ResponseEntity<Object> handleGestureNotFoundException(GestureNotAllowedException ex, WebRequest request) {
		return handleExceptionInternal(ex,
				buildErrorResponse(String.format("Gesture %s not allowed in this context.", ex.getUsedGesture())),
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	private ErrorResponse buildErrorResponse(String message) {
		ErrorResponse response = new ErrorResponse(message);
		response.add(linkTo(GameController.class).withRel("playAgain"));
		return response;
	}
}