package de.meierit.ssp.ssp.application;

import org.springframework.hateoas.ResourceSupport;

import de.meierit.ssp.ssp.domain.Gesture;
import de.meierit.ssp.ssp.domain.Winner;

public class WinningResponse extends ResourceSupport {

	private Winner winner;
	private Gesture humanGesture;
	private Gesture kiGesture;

	public WinningResponse() {
	}

	public WinningResponse(Winner winner, Gesture humanGesture, Gesture kiGesture) {
		this.winner = winner;
		this.humanGesture = humanGesture;
		this.kiGesture = kiGesture;
	}

	public Winner getWinner() {
		return winner;
	}

	public Gesture getHumanGesture() {
		return humanGesture;
	}

	public Gesture getKiGesture() {
		return kiGesture;
	}

}
