package de.meierit.ssp.ssp.application;

import org.springframework.hateoas.ResourceSupport;

public class ErrorResponse extends ResourceSupport {

	private final String message;

	public ErrorResponse(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
