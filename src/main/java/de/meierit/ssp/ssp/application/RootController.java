package de.meierit.ssp.ssp.application;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.util.HashMap;
import java.util.Map;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class RootController {

	@GetMapping
	public RootResource getRootResource() {
		return new RootResource(//
				linkTo(RootController.class).withSelfRel(),
				linkTo(GameController.class).withRel("play"),//
				linkTo(methodOn(GameController.class).playAdvanced(null)).withRel("playAdvanced")); //
	}

	class RootResource extends ResourceSupport {

		public RootResource(Link... links) {
			add(links);
		}

	}

}