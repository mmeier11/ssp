package de.meierit.ssp.ssp.domain;

public class GesturePair {

	private final Gesture gesturePlayerA;
	private final Gesture gesturePlayerB;

	public GesturePair(Gesture gesturePlayerA, Gesture gesturePlayerB) {
		super();
		this.gesturePlayerA = gesturePlayerA;
		this.gesturePlayerB = gesturePlayerB;
	}

	public Gesture getGesturePlayerA() {
		return gesturePlayerA;
	}

	public Gesture getGesturePlayerB() {
		return gesturePlayerB;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gesturePlayerA == null) ? 0 : gesturePlayerA.hashCode());
		result = prime * result + ((gesturePlayerB == null) ? 0 : gesturePlayerB.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GesturePair other = (GesturePair) obj;
		if (gesturePlayerA != other.gesturePlayerA)
			return false;
		if (gesturePlayerB != other.gesturePlayerB)
			return false;
		return true;
	}

}
