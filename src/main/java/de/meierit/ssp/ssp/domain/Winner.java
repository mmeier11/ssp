package de.meierit.ssp.ssp.domain;

public enum Winner {
	
	HUMAN, KI, DRAWN;

}
