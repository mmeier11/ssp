package de.meierit.ssp.ssp.application;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.meierit.ssp.ssp.business.LogicService;
import de.meierit.ssp.ssp.domain.Gesture;
import de.meierit.ssp.ssp.domain.Winner;

@RunWith(SpringRunner.class)
@WebMvcTest(GameController.class)
public class GameControllerTest {

	private static final String GAME_URL = "/api/game";

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@InjectMocks
	private GameController controller;

	@MockBean
	private LogicService logicService;

	@Autowired
	private MockMvc mockMvc;

	@Captor
	private ArgumentCaptor<Gesture> gestureCaptor;

	@Test
	public void playWithScissors() throws Exception {
		playtest(Gesture.SCISSORS);
	}

	@Test
	public void playWithPaper() throws Exception {
		playtest(Gesture.PAPER);
	}

	@Test
	public void playWithRock() throws Exception {
		playtest(Gesture.ROCK);
	}

	private void playtest(Gesture gesture) throws Exception {
		Mockito.when(logicService.playBasic(Mockito.any(Gesture.class)))
				.thenReturn(new WinningResponse(Winner.DRAWN, Gesture.SCISSORS, Gesture.PAPER));

		mockMvc.perform(post(GAME_URL).content(buildContent(gesture)).contentType(contentType))//
				.andExpect(status().isOk());

		Mockito.verify(logicService).playBasic(gestureCaptor.capture());
		assertThat(gestureCaptor.getValue(), equalTo(gesture));
	}

	private String buildContent(Gesture gesture) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(new Play(gesture));
	}

}
