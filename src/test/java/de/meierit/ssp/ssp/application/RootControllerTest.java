package de.meierit.ssp.ssp.application;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(RootController.class)
public class RootControllerTest {

	private static final String GAME_URL = "/api/game";
	private static final String ADVANCED_GAME_URL = "/api/game/advanced";
	private static final String BASE_URL = "/api";
	private static final String EXP_HOST = "http://localhost";

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getRootResource() throws Exception {

		mockMvc.perform(get(BASE_URL).accept(contentType))//
				.andExpect(status().isOk())//
				.andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$._links.self.href").value(EXP_HOST + BASE_URL))//
				.andExpect(jsonPath("$._links.play.href").value(EXP_HOST + GAME_URL))//
				.andExpect(jsonPath("$._links.playAdvanced.href").value(EXP_HOST + ADVANCED_GAME_URL));

	}

}
