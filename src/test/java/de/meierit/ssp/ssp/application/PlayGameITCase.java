package de.meierit.ssp.ssp.application;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.meierit.ssp.ssp.domain.Gesture;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PlayGameITCase {

	private static final String GAME_URL = "/api/game";
	private static final String ADVANCED_GAME_URL = "/api/game/advanced";

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void playBasicReturnResponse() throws Exception {
		MvcResult result = mockMvc.perform(post(GAME_URL).content(buildScissorsPlayContent()).contentType(contentType))//
				.andExpect(status().isOk()).andReturn();
		WinningResponse response = mapResponse(result.getResponse().getContentAsString());
		assertThat(response.getHumanGesture(), equalTo(Gesture.SCISSORS));
		assertThat(response.getKiGesture(), notNullValue());
		assertThat(response.getWinner(), notNullValue());
	}

	@Test
	public void playBasicWithWellShouldReturnBadGatewayCode() throws JsonProcessingException, Exception {
		mockMvc.perform(post(GAME_URL).content(buildWellPlayContent()).contentType(contentType))//
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void playAdvancedWithReturnResponse() throws JsonProcessingException, Exception {
		mockMvc.perform(post(ADVANCED_GAME_URL).content(buildWellPlayContent()).contentType(contentType))//
				.andExpect(status().isOk());
	}

	private String buildScissorsPlayContent() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(new Play(Gesture.SCISSORS));
	}

	private String buildWellPlayContent() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(new Play(Gesture.WELL));
	}

	private WinningResponse mapResponse(String content) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(content, WinningResponse.class);
	}

}
