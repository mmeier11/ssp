package de.meierit.ssp.ssp.business;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;	
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import de.meierit.ssp.ssp.domain.Gesture;

@RunWith(MockitoJUnitRunner.class)
public class LogicBuilderTest {
	
	@Mock
	private LogicHolder holder;
	
	@Captor
	private ArgumentCaptor<Gesture> winningGesture;
	
	@Test
	public void testIfLogicIsAddedTwice() {
		LogicBuilder builder = 	new LogicBuilder(holder, Gesture.SCISSORS);
		
		builder.beats(Gesture.PAPER).add();
		
		Mockito.verify(holder, times(2)).addLogic(any(Gesture.class), any(Gesture.class), any(Gesture.class));
	}
	
	@Test
	public void testIfWinningGestureIsCorrect() {
		LogicBuilder builder = 	new LogicBuilder(holder, Gesture.SCISSORS);
		
		builder.beats(Gesture.PAPER).add();
		
		Mockito.verify(holder, times(2)).addLogic(any(Gesture.class), any(Gesture.class), winningGesture.capture());
		assertThat(winningGesture.getAllValues(), hasItem(Gesture.SCISSORS));
	}

}
