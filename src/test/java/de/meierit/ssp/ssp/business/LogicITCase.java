package de.meierit.ssp.ssp.business;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;

import java.util.EnumSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.yaml.snakeyaml.DumperOptions.ScalarStyle;

import de.meierit.ssp.ssp.application.WinningResponse;
import de.meierit.ssp.ssp.domain.Gesture;
import de.meierit.ssp.ssp.domain.Winner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogicITCase {

	@Autowired
	@Spy
	private LogicService service;

	@Test
	public void playScissorsAgainstRock() {
		logicTest(Gesture.SCISSORS, Gesture.ROCK, Winner.KI);
	}

	@Test
	public void playScissorsAgainstPaper() {
		logicTest(Gesture.SCISSORS, Gesture.PAPER, Winner.HUMAN);
	}

	@Test
	public void playScissorsAgainstScissors() {
		logicTest(Gesture.SCISSORS, Gesture.SCISSORS, Winner.DRAWN);
	}
	
	@Test
	public void playPaperAgainstScissors() {
		logicTest(Gesture.PAPER, Gesture.SCISSORS, Winner.KI);
	}
	
	@Test
	public void playPaperAgainstRock() {
		logicTest(Gesture.PAPER, Gesture.ROCK, Winner.HUMAN);
	}
	
	@Test
	public void playPaperAgainstPaper() {
		logicTest(Gesture.PAPER, Gesture.PAPER, Winner.DRAWN);
	}
	
	@Test
	public void playRockAgainstPaper() {
		logicTest(Gesture.ROCK, Gesture.PAPER, Winner.KI);
	}
	
	@Test
	public void playRockAgainstScissors() {
		logicTest(Gesture.ROCK, Gesture.SCISSORS, Winner.HUMAN);
	}
	
	@Test
	public void playRockAgainstRock() {
		logicTest(Gesture.ROCK, Gesture.ROCK, Winner.DRAWN);
	}

	private void logicTest(Gesture human, Gesture ki, Winner expectedWinner) {
		setKiGesture(ki);
		WinningResponse result = service.playBasic(human);
		assertThat(result.getWinner(), equalTo(expectedWinner));
	}

	private void setKiGesture(Gesture kiGesture) {
		Mockito.doReturn(kiGesture).when(service).getKiGesture(any(EnumSet.class));
	}

}
