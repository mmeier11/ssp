package de.meierit.ssp.ssp.business;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;

import java.util.EnumSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import de.meierit.ssp.ssp.application.WinningResponse;
import de.meierit.ssp.ssp.domain.Gesture;
import de.meierit.ssp.ssp.domain.Winner;

@RunWith(MockitoJUnitRunner.class)
public class LogicServiceTest {
	
	@Mock
	private LogicHolder holder;
	
	@InjectMocks
	@Spy
	private LogicService service;
	
	@SuppressWarnings("unchecked")
	@Test
	public void humanWinning() {
		Mockito.doReturn(Gesture.PAPER).when(service).getKiGesture(any(EnumSet.class));
		Mockito.when(holder.getWinningGestureOf(any(), any())).thenReturn(Gesture.SCISSORS);
		
		WinningResponse winningPlayer =  service.playBasic(Gesture.SCISSORS);
		assertThat(winningPlayer.getWinner(), equalTo(Winner.HUMAN));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void kiWinning() {
		Mockito.doReturn(Gesture.ROCK).when(service).getKiGesture(any(EnumSet.class));
		Mockito.when(holder.getWinningGestureOf(any(), any())).thenReturn(Gesture.ROCK);
		
		WinningResponse winningPlayer =  service.playBasic(Gesture.SCISSORS);
		assertThat(winningPlayer.getWinner(), equalTo(Winner.KI));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void drawn() {
		Mockito.doReturn(Gesture.SCISSORS).when(service).getKiGesture(any(EnumSet.class));
		
		WinningResponse winningPlayer =  service.playBasic(Gesture.SCISSORS);
		assertThat(winningPlayer.getWinner(), equalTo(Winner.DRAWN));
	}
	
	@Test(expected=GestureNotAllowedException.class)
	public void useNotAllowedGesture() {
		service.playBasic(Gesture.WELL);
	}
	
	@Test
	public void playWellInAdvancedModeOk() {
		WinningResponse response = service.playAdvanced(Gesture.WELL);
		assertThat(response, notNullValue());
	}

}
