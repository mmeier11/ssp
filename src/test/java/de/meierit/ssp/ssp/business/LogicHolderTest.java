package de.meierit.ssp.ssp.business;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;	

import de.meierit.ssp.ssp.domain.Gesture;

@RunWith(MockitoJUnitRunner.class)
public class LogicHolderTest {

	@Spy
	private LogicHolder logicHolder = new LogicHolder();
	
	@Captor
	private ArgumentCaptor<Gesture> first;
	
	@Captor
	private ArgumentCaptor<Gesture> second;
	
	@Captor
	private ArgumentCaptor<Gesture> winning;

	@Test
	public void addLogicForScissors() {
		testLogicWith(Gesture.SCISSORS, Gesture.PAPER);
	}
	
	@Test
	public void addLogicForPaper() {
		testLogicWith(Gesture.PAPER, Gesture.ROCK);
	}
	
	@Test
	public void addLogicForRock() {
		testLogicWith(Gesture.ROCK, Gesture.SCISSORS);
	}
	
	@Test
	public void addLogicForWell() {
		testLogicWith(Gesture.WELL, Gesture.SCISSORS);
	}


	private void testLogicWith(Gesture win, Gesture los) {
		logicHolder.buildFor(win).beats(los).add();
		
		Mockito.verify(logicHolder, times(2)).addLogic(first.capture(), second.capture(), winning.capture());
		assertThat(first.getAllValues(), hasItems(win, los));
		assertThat(second.getAllValues(), hasItems(win, los));
		assertThat(winning.getAllValues(), hasItem(win));
		assertThat(winning.getAllValues(), not(hasItems(los)));
	}

	
}
