# Code Kata: Schere, Stein, Papier

## Start der Applikation

Der Start erfolgt über:
```sh
$ java -jar ssp-0.0.1-SNAPSHOT.jar
```

Das Basis-Spiel enthält nur Schere(SCISSORS), Stein(ROCK) und Papier(PAPER). Das Advanced-Spiel enthält zusätzlich noch den Brunnen(WELL).
Sollte im Basis-Spiel der Brunnen verwendet werden, wird eine Fehlermeldung zurück gegeben.

Viel Spaß beim Spielen!

## Postman

Dem Projekt liegt eine Postman (https://www.getpostman.com/) Collection (SSP.postman_collection.json) bei, über die die REST Endpoints bedient werden können.


## API Beispiele

Das API Root ist http://localhost:8080/api

```
GET http://localhost:8080/api
{
    "_links": {
        "self": {
            "href": "http://localhost:8080/api"
        },
        "play": {
            "href": "http://localhost:8080/api/game"
        },
        "playAdvanced": {
            "href": "http://localhost:8080/api/game/advanced"
        }
    }
}

POST http://localhost:8080/api/game
{
	"gesture":"SCISSORS"
}

{
    "winner": "KI",
    "humanGesture": "SCISSORS",
    "kiGesture": "ROCK"
}

POST http://localhost:8080/api/game/advanced
{
	"gesture":"WELL"
}

{
    "winner": "HUMAN",
    "humanGesture": "WELL",
    "kiGesture": "ROCK"
}

```

## Fehlerhandling

```
Fehlerfall:
{
    "message": "Gesture WELL not allowed in this context.",
    "links": [
        {
            "rel": "playAgain",
            "href": "http://localhost:8080/api/game"
        }
    ]
}
```
